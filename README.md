# Containers from scratch

This repo provides a demo on how to build Containers from scratch.

If you are using Linux you can just start over. On macOS and Windows you can start a Alpine container as demo environemnt:
```
docker run --rm -d --name demo --privileged docker:dind
docker container exec -it demo sh
```

We now have a linux environment including a Docker daemon. Let's start a container:
```
docker container run -ti --name our-container alpine:latest
```

We now use the container image as basis for our own image:
```
mkdir -p /tmp/our-container
docker container export our-container | tar xfx - -C /tmp/our-container
```

Let's play with `chroot` and change some files in the filesystem:
```
chroot /tmp/our-container sh
```

And now with Linux namespaces and `unshare`. Review your processes and hostname and network after executing the command:
```
unshare -munpf -i chroot /tmp/our-container
```

How layered container images work. Create files in the merged space and check the other directores:
```
mkdir /tmp/lower /tmp/upper /tmp/work /tmp/merged
touch /tmp/lower/test
mount -t overlay overlay -o lowerdir=/tmp/lower,upperdir=/tmp/upper,workdir=/tmp/work /tmp/merged
```

And there is even more. Cgroups, network, mount, ...

